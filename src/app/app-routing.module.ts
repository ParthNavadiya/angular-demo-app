import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './Auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './Modules/Dashboard/dashboard.module#DashBoardModule',
    canActivate: [AuthGuard],
  },
  {
    path: '',
    loadChildren: './Modules/auth/auth.module#AuthModule',
  },

  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
