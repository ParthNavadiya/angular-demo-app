import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authToken!: string;
  baseURL = environment.BASE_API_URL;
  constructor(private http: HttpClient) {}
  login(username: string, password: string) {
    return this.http
      .post<any>(`${this.baseURL}users/authenticate`, {
        username: username,
        password: password,
      })
      .pipe(
        map((user: { token: any }) => {
          if (user && user.token) {
            this.setToken(user);
          }

          return user;
        })
      );
  }

  // get local storage token
  getToken(): string | null {
    if (this.authToken) {
      return this.authToken;
    } else {
      return localStorage.getItem('token')
        ? localStorage.getItem('token')
        : null;
    }
  }
  
  // get local storage currentUser
  getCurrentUser(): string | null {
    return localStorage.getItem('currentUser')
      ? localStorage.getItem('currentUser')
      : null;
  }

  // set token in localstorage
  setToken(user: any): void {
    this.authToken = user.token;
    localStorage.setItem('token', user.token);
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  logout() {
    this.removeLocalStorageItem();
  }

  removeLocalStorageItem() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
  }
}
