import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../Models/User';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  baseURL = environment.BASE_API_URL;
  constructor(private http: HttpClient) {}

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseURL}users/`);
  }

  getById(id: string): Observable<User> {
    return this.http.get<User>(`${this.baseURL}users/` + id);
  }

  register(user: User) {
    return this.http.post(`${this.baseURL}users/register`, user);
  }

  update(user: User) {
    return this.http.put(`${this.baseURL}users/` + user.id, user);
  }

  delete(id: string) {
    return this.http.delete(`${this.baseURL}users/` + id);
  }

  uploadProfileImage(image: any, id: any) {
    return this.http.post(`${this.baseURL}users/${id}/profile`, image);
  }
}
