import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyInterceptor } from './Class/interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SettingsComponentComponent } from './Components/settings-component/settings-component.component';

import { AuthModule, DashBoardModule } from './Modules';

import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { EditUserComponentComponent } from './Components/edit-user-component/edit-user-component.component';

@NgModule({
  declarations: [
    AppComponent,
    SettingsComponentComponent,
    EditUserComponentComponent,
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule, 
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    DashBoardModule,
    SocialLoginModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MyInterceptor, multi: true },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '426077662527-um2euk02sa3m2tl5lbhrvcifcpi9iv3c.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('clientId'),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
