import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/Models/User';
import { UserService } from 'src/app/services/user.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-edit-user-component',
  templateUrl: './edit-user-component.component.html',
  styleUrls: ['./edit-user-component.component.css'],
})
export class EditUserComponentComponent implements OnInit {
  user!: User;
  userId: string;
  loading = false;
  submitted = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {
    this.userId = '';
    this.user = {
      id: '',
      firstName: '',
      lastName: '',
      username: '',
    };
  }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.userId = id;
      this.userService.getById(id).subscribe((userData) => {
        if (userData) {
          this.user = userData;
        }
      });
    }
  }

  onSubmit(editForm: NgForm): void {
    console.log('user', editForm);
    if (!editForm.valid) {
      return;
    }

    this.loading = true;
    const userData = { ...editForm.value, id: this.userId };
    this.userService
      .update(userData)
      .pipe(first())
      .subscribe(
        (data) => {
          alert('Edit successful');
          this.router.navigate(['/home']);
        },
        (error) => {
          console.log('error', error);
          // alert(error);
          this.loading = false;
        }
      );
  }
}
