import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from 'src/app/Models/User';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  loading = false;
  submitted = false;
  uploadedFiles!: File;
  imageURL: string | ArrayBuffer | null = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get registerFormControl() {
    return this.registerForm.controls;
  }

  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.uploadedFiles = <File>event.target.files[0];
      var reader = new FileReader();

      reader.onload = () => {
        // called once readAsDataURL is completed
        this.imageURL = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]); // read file as data url
    }
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    let formData = new FormData();
    if (this.uploadedFiles) {
      formData.append(
        'uploadImage',
        this.uploadedFiles,
        this.uploadedFiles.name
      );
    }
    this.userService
      .register(this.registerForm.value)
      .pipe(first())
      .subscribe(
        (data) => {
          console.log('data', data);
          // this.userService.uploadProfileImage(formData, data.id).subscribe((res) => {
          //   console.log('Registration successful', data);
          this.router.navigate(['/login']);
          // });
        },
        (error) => {
          alert(error);
          this.loading = false;
        }
      );
  }
}
