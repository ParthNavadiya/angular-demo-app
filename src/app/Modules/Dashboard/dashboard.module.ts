import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponentComponent } from '../../Components/home-component/home-component.component';
import { EditUserComponentComponent } from '../../Components/edit-user-component/edit-user-component.component';

export const routes: Routes = [
  { path: 'home', component: HomeComponentComponent },
  { path: 'home/:id', component: EditUserComponentComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  declarations: [HomeComponentComponent],
  entryComponents: [],
})
export class DashBoardModule {}
